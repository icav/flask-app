
## Overview

- Python
  - [Flask](https://flask.palletsprojects.com/en/1.1.x/) is our web framework.
- This project returns two APIs contains data stored in csv file:
  - we have a csv file containing list of books.
  - We are fetching data from the csv using pandas.
  - We are returning all the data in the default home page.
  - Our first api returns books that are requested by the user.
  - Our second api filter the data from the csv.
- To get the required response using 1st API:
  - [First_API_URL](http://127.0.0.1:8000/inputrows/rows=)
  - Enter the number of rows we want in return.
- To get the required response using 2nd API:
  - [Second_API_URL](http://127.0.0.1:8000/filterdata/{specify_column}={value})
  - Enter correct column name and value.




### Local setup

- Clone the repo locally.
- To run api locally you will need run following command:
    - cd api
    - python3 run.py
    - pip install -r requirements.txt ( to install all the packages)

### Feature development

- Create a new branch from `main` branch.
- Make your changes in this branch.
- Create a merge request back to `main` branch.
- Once approved and pipelines pass, squash merge the merge request into main branch.
- Create new branch if there are tasks left or if new feedback comes for the issue.



## Notes

- how to add new api:

- add new file in api,src folder for ex(file.py)
- define the function in the file
- import that file in the main run.py file.
- make sure to define the route in the run.py function.
- after that add tests for the new api.
- create tests in tests.py file for the new api.