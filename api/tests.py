from run import app
import json
import random


def test_get_all_books():
    response = app.test_client().get("/")
    assert response.status_code == 200


def test_get_expected_books():
    var = random.randint(0, 50)
    response = app.test_client().get(f"/inputrows/rows={var}")
    res = json.loads(response.data.decode("utf-8")).get("Books")
    assert response.status_code == 200
    assert len(res) == var


def test_get_filtered_books():
    response = app.test_client().get(f"/filterdata/author=Barbara Parisi")
    assert response.status_code == 200
