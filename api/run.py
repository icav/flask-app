from flask import Flask, abort
from src.getrows import (
    handle_get_csvdata,
    handle_get_input_rows,
    handle_get_filter_data,
)

app = Flask(__name__)


@app.route("/")
def csv_to_json():
    ret = handle_get_csvdata()
    return ret


@app.route("/inputrows/rows=<max_len>")
def return_input_rows(max_len):
    ret = handle_get_input_rows(int(max_len))
    return ret


@app.route("/filterdata/<column>=<value>")
def return_filter_data(column, value):
    ret = handle_get_filter_data(column, value)
    return ret


@app.errorhandler(404)
def not_found(error):
    return "404 error", 404


@app.errorhandler(403)
def unauth_page(e):
    # pylint: disable=unused-argument
    return {"ok": False, "reason": "Please specify correct values", "code": 403}, 200


if __name__ == "__main__":
    app.run(host="localhost", port=8000, debug=True)
