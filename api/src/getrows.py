import pandas as pd
from flask import Flask, abort

import ujson as json


def handle_get_csvdata():
    df = pd.read_csv("data/books.csv")
    ret = {}
    ret["Books"] = df.to_dict(orient="records")
    return ret


def handle_get_input_rows(max_len):
    ret = handle_get_csvdata()
    value = {}
    exact_value = ret["Books"][:max_len]
    value["Books"] = exact_value
    return json.dumps(value, indent=4)


def handle_get_filter_data(column, value):
    if value is None:
        raise abort(403)
    ret = handle_get_csvdata()
    exact = []
    demodict = {}
    for key in ret["Books"]:
        try:
            if key[column] == value:
                exact.append(key)
                demodict["Books"] = exact
        except Exception:
            raise abort(403)
    return json.dumps(demodict, indent=4)
